import FormCreate from '@form-create/element-ui'
import { ElSelectV2, ElButton, ElUpload, ElSwitch, ElMessageBox, ElMessage } from 'element-plus/lib/index'
import ElementUI from 'element-plus/es/index'
import UploadIcon from '@/components/UploadIcon/UploadIcon.vue'
import UploadIconOrInput from '@/components/UploadIcon/UploadIconOrInput.vue'
import UploadIconBatch from '@/components/UploadIcon/UploadIconBatch.vue'
import RichEditor from '@/components/Editor/index.vue'
import VXETable from 'vxe-table'
import axios from 'axios'

import 'xe-utils'
import 'element-plus/dist/index.css'
import 'vxe-table/lib/style.css'
import bus from './bus'

export const install = (app, router) => {
    app.use(VXETable)
    app.use(ElementUI)
    app.use(FormCreate)
    app.use(router)

    /**
     * 删除二次确认弹窗
     * @param {} type
     * @param {*} row
     */
    app.config.globalProperties.deleteItByRow = (type, row) => {
        ElMessageBox.confirm('删除后不可复原，是否确定删除?', '提示', {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type: 'warning'
        }).then(() => {
            axios
                .post('/download/' + type + '/delete', { id: row.id })
                .then(res => {
                    bus.emit('reload')
                    ElMessage({
                        type: 'success',
                        message: '删除成功!'
                    })
                })
        }).catch(error => {
            if (error !== 'cancel') {
                ElMessage({
                    type: 'danger',
                    message: '删除失败!' + error
                })
            }
         
        })
    }

    /**
     * 编辑
     * @param {*} type 
     * @param {*} row 
     */
    app.config.globalProperties.editItByRow = (type, row) => {
        // console.log(router);
        router.push(`/${type}?id=${row.id}`)
    }

}

FormCreate.component('ElSelectV2', ElSelectV2)
FormCreate.component('ElButton', ElButton)
FormCreate.component('UploadIcon', UploadIcon)
FormCreate.component('UploadIconBatch', UploadIconBatch)
FormCreate.component('ElUpload', ElUpload)
FormCreate.component('ElSwitch', ElSwitch)
FormCreate.component('RichEditor', RichEditor)
FormCreate.component('UploadIconOrInput', UploadIconOrInput)

export const vxeGridConfig = (
    ajax,
    result = 'data.data.record',
    total = 'data.data.total'
) => ({
    autoResize: true,
    border: 'full',
    round: true,
    showHeaderOverflow: true,
    showOverflow: true,
    highlightHoverRow: true,
    keepSource: true,
    id: 'xGrid',
    height: '640px',
    size: 'mini',
    rowId: 'id',
    pagerConfig: {
        autoHidden: true,
        pageSize: 20,
        pageSizes: [5, 10, 15, 20, 50, 100, 200, 500, 1000]
    },
    proxyConfig: {
        seq: true, // 启用动态序号代理，每一页的序号会根据当前页数变化
        sort: true, // 启用排序代理，当点击排序时会自动触发 query 行为
        filter: true, // 启用筛选代理，当点击筛选时会自动触发 query 行为
        form: true, // 启用表单代理，当点击表单提交按钮时会自动触发 reload 行为
        props: {
            result: result, // 配置响应结果列表字段
            total: total // 配置响应结果总页数字段
        },
        ajax: {
            query: ajax
        }
    }
})
