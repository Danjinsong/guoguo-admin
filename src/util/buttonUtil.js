/**
 * 增强上传事件，构造一个上传选择文件的行为
 * @returns 点击任意按钮上传
 */
export const openUpload = async function(accept = '*') {
    return new Promise((resolve, reject) => {
        var inputObj = document.createElement('input')
        inputObj.setAttribute('id', 'file_' + new Date().getTime())
        inputObj.setAttribute('type', 'file')
        inputObj.setAttribute('accept', accept)
        inputObj.setAttribute('style', 'visibility:hidden')
        document.body.appendChild(inputObj)
        inputObj.onchange = input => {
            // const jt = gb.$loading({
            //   fullscreen: true,
            //   text: '文件处理中，请稍后！',
            //   lock: true,
            //   body: true,
            // })
            if (window.FileReader) {
                // jt.close()
                resolve(inputObj.files)
            } else {
                console.log(input)
                reject(new Error('无法读取文件!'))
            }
        }
        inputObj.click()
    })
}
