const Layout = () => import('@/layout/index.vue')

export default [
    {
        path: '/tag',
        component: Layout,
        redirect: '/tag/index',
        name: 'tag',
        meta: {
            title: '标签管理',
            icon: 'sidebar-jsx'
        },
        children: [
            {
                path: 'index',
                name: 'TagIndex',
                component: () => import('@/views/download/Tag.vue'),
                meta: {
                    title: '列表',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/tag'
                }
            },
            {
                path: 'editTag',
                name: 'EditTag',
                component: () => import('@/views/download/EditTag.vue'),
                meta: {
                    title: '详情',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/tag'
                }
            }
        ]
    },
    {
        path: '/zd',
        component: Layout,
        redirect: '/zd/index',
        name: 'zd',
        meta: {
            title: '字典管理',
            icon: 'sidebar-jsx'
        },
        children: [
            {
                path: 'index',
                name: 'ZdIndex',
                component: () => import('@/views/download/Zd.vue'),
                meta: {
                    title: '列表',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/zd'
                }
            },
            {
                path: 'editZd',
                name: 'EditZd',
                component: () => import('@/views/download/EditZd.vue'),
                meta: {
                    title: '详情',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/zd'
                }
            }
        ]
    },
    {
        path: '/lx',
        component: Layout,
        redirect: '/lx/index',
        name: 'lx',
        meta: {
            title: '类型管理',
            icon: 'sidebar-jsx'
        },
        children: [
            {
                path: 'index',
                name: 'LxIndex',
                component: () => import('@/views/download/Lx.vue'),
                meta: {
                    title: '列表',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/lx'
                }
            },
            {
                path: 'editLx',
                name: 'EditLx',
                component: () => import('@/views/download/EditLx.vue'),
                meta: {
                    title: '详情',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/lx'
                }
            },
            {
                path: 'guanlian',
                name: 'Guanlian',
                component: () => import('@/views/download/Guanlian.vue'),
                meta: {
                    title: '树',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/lx'
                }
            }
        ]
    },
    {
        path: '/rj',
        component: Layout,
        redirect: '/rj/index',
        name: 'rj',
        meta: {
            title: '软件管理',
            icon: 'sidebar-jsx'
        },
        children: [
            {
                path: 'index',
                name: 'RjIndex',
                component: () => import('@/views/download/Rj.vue'),
                meta: {
                    title: '列表',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/rj'
                }
            },
            {
                path: 'editRj',
                name: 'EditRj',
                component: () => import('@/views/download/EditRj.vue'),
                meta: {
                    title: '详情',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/rj'
                }
            }
        ]
    },
    {
        path: '/zx',
        component: Layout,
        redirect: '/zx/index',
        name: 'zx',
        meta: {
            title: '资讯管理',
            icon: 'sidebar-jsx'
        },
        children: [
            {
                path: 'index',
                name: 'ZxIndex',
                component: () => import('@/views/download/Zx.vue'),
                meta: {
                    title: '列表',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/zx'
                }
            },
            {
                path: 'editZx',
                name: 'EditZx',
                component: () => import('@/views/download/EditZx.vue'),
                meta: {
                    title: '详情',
                    sidebar: false,
                    breadcrumb: true,
                    activeMenu: '/zx'
                }
            }
        ]
    }
]
